const co = require('co');
const fs = require('fs');

const readFile = url => (
  new Promise((resolve, reject) => {
    fs.readFile(url, 'utf-8', (err, data) => {
      if (err) reject(err);
      resolve(data);
    })
  })
)

co(function* () {
  const result = yield readFile('./data1.txt');
  return result;
}).then(result => {
  console.log(result)
}).catch(console.log)
